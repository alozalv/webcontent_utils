#!/usr/bin/env python2.7

'''
This module handles Web Content Pages
'''
import argparse, sys, ConfigParser, logging, os
from utils.constants import LOG_FILE, LOG_FORMAT, OUTPUT_DIR, SECTION_GENERAL, URL
import pdfkit
from datetime import datetime

class WebContentManager(object):
    '''
    This class handles Web Content
    '''

    output_dir = None

    def __init__(self, initparams):
        logging.basicConfig(
            filename=initparams[LOG_FILE],
            level=logging.DEBUG,
            format=LOG_FORMAT)

        self.output_dir = initparams[OUTPUT_DIR]


    def export_page(self, url, output_file):
        '''
        Export the provided page as PDF
        '''
        logging.info(u"Exporting page [{0}]: ".format(url))
        pdfkit.from_url(url, output_file)



def parse_config_file(inifile):
    '''
    Parse .ini configuration file at inifile
    '''
    result = {}
    try:
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(inifile)
        result = {
            LOG_FILE:
            cfgparser.get(SECTION_GENERAL, LOG_FILE),

            OUTPUT_DIR:
            cfgparser.get(SECTION_GENERAL, OUTPUT_DIR)
            }
    except Exception:
        raise IOError("Unable to read [{0}].\
            Check that the configuration file exists and has a valid format"
            .format(inifile))
    return result

def replace_defaults_with_arguments(defaults, arguments):
    '''
    Taking the dictionary defaults as reference,
    replace (or add if necessary) any value for the matching keys in arguments
    '''    
    params = defaults
    for param in arguments:
        if arguments[param] is not None:
            params[param] = arguments[param]

    return params


def parse_arguments():
    '''
    This function parses command line parameters
    and load all the necessary information from a config file
    '''

    parser = argparse.ArgumentParser( description = \
        "Create or update MediaWiki pages.\
        Handles content as hypertext, but also page attachments.")

    parser.add_argument('-u', '--url', action = "store",
        dest = URL,
        help = "Web page to export as PDF")

    parser.add_argument('-f', '--configfile', action = "store",
        dest="inifile",
        help = "Configuration parameters provided inside a .ini file. \
        These parameters will be overwritten by inline parameters")

    args  = parser.parse_args(sys.argv[1:])
    
    cfgparams = {}
    if args.inifile is not None:
        # Load the defaults in the .ini file
        cfgparams = parse_config_file(args.inifile)

    # Replace defaults with the parameters provided in the command line
    parameters = replace_defaults_with_arguments(cfgparams, vars(args))

    return parameters

if __name__ == '__main__':
    ARGS = parse_arguments()

    # Instantiate the gdoc manager with the provided credentials
    MANAGER = WebContentManager(ARGS)

    MANAGER.export_page(
        url = ARGS[URL],
        #output_file = os.path.join(ARGS[OUTPUT_DIR], str(datetime.now()).replace(':', '')+".pdf")
        output_file = os.path.join(ARGS[OUTPUT_DIR], "hola.pdf")
    )

    print("== DONE! ==")
