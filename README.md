# Web Content utils #

> ### Disclaimer: WIP ###
>
> The code in this repo is continous **work in progress** to learn about python and ways to download web content as PDF.
> Feel free to use some of it, but take always the **master** branch as a reference.
>
> ***Leave your questions and feedback as new [issues](https://bitbucket.org/alozalv/webcontent_utils/issues).***
>
>
> Note that [pdfkit](https://pypi.python.org/pypi/pdfkit/) needs to be installed in your system.
>
> Thanks!

This repository contains a number of modules to handle download web content as PDF.

Inside the *webcontent_utils* folder, you'll find:

+ *webcontent_manager.py*: WebContentManager class. Made runnable for examplary purposes (see below).
+ *config*: configuration folder. Copy the sample and replace the values on it with your own.
+ *utils*: some utility modules such as constants.

You can run *webcontent_manager.py* in order to download a web page as PDF.


```
./webcontent_manager.py -h                               
usage: webcontent_manager.py [-h] [-u URL] [-f INIFILE]

Create or update MediaWiki pages. Handles content as hypertext, but also page
attachments.

optional arguments:
  -h, --help            show this help message and exit
  -u URL, --url URL     Web page to export as PDF
  -f INIFILE, --configfile INIFILE
                        Configuration parameters provided inside a .ini file.
                        These parameters will be overwritten by inline
                        parameters
```
